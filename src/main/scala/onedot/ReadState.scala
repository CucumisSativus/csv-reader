package onedot

case class ReadState(
    currentField: Field,
    possibleQuotation: String,
    possibleDelimiter: String,
    possibleLineSeparator: String,
    quotingState: QuotingState,
    eofReached: Boolean)

object ReadState {
  val Empty: ReadState =
    ReadState(
      currentField = Field(""),
      possibleQuotation = "",
      possibleDelimiter = "",
      possibleLineSeparator = "",
      quotingState = QuotingState.Empty,
      eofReached = false)

  implicit class ReadElementStateOps(val currentState: ReadState) extends AnyVal {
    def appendNewCharacter(newCharacter: Char)(implicit parsingParameters: CsvParsingParameters): ReadState = {
      val quotation = newPossibleQuotation(newCharacter)

      val field = currentState.currentField.appendCharacter(newCharacter)

      val newQuotingState =
        if (quotation == parsingParameters.quotingString) currentState.quotingState.quoteEncounteredAt(field.length - 1)
        else currentState.quotingState

      val delimiter = {
        val delimiterWithNewCharacter = currentState.possibleDelimiter.appended(newCharacter)
        if (!newQuotingState.isInQuotation && isPossibleDelimiter(delimiterWithNewCharacter)) delimiterWithNewCharacter
        else ""
      }

      val lineSeparator = {
        val lineSeparatorWithNewCharacter = currentState.possibleLineSeparator.appended(newCharacter)
        if (!newQuotingState.isInQuotation && possibleLineSeparator(lineSeparatorWithNewCharacter))
          lineSeparatorWithNewCharacter
        else ""
      }

      ReadState(field, quotation, delimiter, lineSeparator, newQuotingState, eofReached = false)
    }

    def updateField(field: Field): ReadState = currentState.copy(currentField = field)

    def isLineTerminated(implicit parsingParameters: CsvParsingParameters): Boolean =
      currentState.eofReached || NewLineReached.newLineReached(currentState)

    private def newPossibleQuotation(newCharacter: Char)(implicit parsingParameters: CsvParsingParameters) = {
      if (possibleQuotation(currentState.possibleQuotation.appended(newCharacter)))
        currentState.possibleQuotation.appended(newCharacter)
      else ""
    }

  }

  private def isPossibleDelimiter(currentDelimiter: String)(implicit parameters: CsvParsingParameters): Boolean = {
    parameters.fieldDelimiter.startsWith(currentDelimiter)
  }
  private def possibleLineSeparator(currentLineSeparator: String)(implicit
      parameters: CsvParsingParameters): Boolean = {
    parameters.lineSeparator.startsWith(currentLineSeparator)
  }
  private def possibleQuotation(currentQuote: String)(implicit parameters: CsvParsingParameters): Boolean = {
    parameters.quotingString.startsWith(currentQuote)
  }

  object DelimiterReached {
    def unapply(currentState: ReadState)(implicit parsingParameters: CsvParsingParameters): Option[Field] = {
      if (!currentState.quotingState.isInQuotation && currentState.possibleDelimiter == parsingParameters.fieldDelimiter) {
        Some(currentState.currentField.dropDelimiter)
      } else {
        None
      }
    }
  }

  object NewLineReached {
    def newLineReached(currentState: ReadState)(implicit parsingParameters: CsvParsingParameters): Boolean = {
      !currentState.quotingState.isInQuotation && currentState.possibleLineSeparator == parsingParameters.lineSeparator
    }
    def unapply(currentState: ReadState)(implicit parsingParameters: CsvParsingParameters): Option[Field] = {
      if (newLineReached(currentState)) {
        Some(currentState.currentField.dropNewLine)
      } else {
        None
      }
    }
  }
}
