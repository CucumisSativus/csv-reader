package onedot

case class MyCsvContent(header: Option[LazyList[String]], rows: LazyList[LazyList[String]]) extends CsvContent
