package onedot

case class QuotingState(quoteOpenedAt: Option[Int], quoteClosedAt: Option[Int]) {
  def quoteEncounteredAt(index: Int): QuotingState = {
    if (quoteOpenedAt.isEmpty) copy(quoteOpenedAt = Some(index))
    else copy(quoteClosedAt = Some(index))
  }

  def isInQuotation: Boolean = quoteOpenedAt.nonEmpty && quoteClosedAt.isEmpty
}

object QuotingState {
  val Empty = QuotingState(None, None)
}
