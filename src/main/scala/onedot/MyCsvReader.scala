package onedot
import java.io.{ BufferedReader, File, FileInputStream, InputStreamReader }

import scala.annotation.tailrec

class MyCsvReader extends CsvReader {
  override def read(path: File, parsingParameters: CsvParsingParameters): CsvContent = {
    implicit val parameters: CsvParsingParameters = parsingParameters
    val reader = new BufferedReader(new InputStreamReader(new FileInputStream(path), parameters.charset))

    def readLine: LazyList[String] = {
      @tailrec
      def readField(previousState: ReadState): ReadState = {
        val readResult = reader.read()
        if (readResult == -1) previousState.copy(eofReached = true)
        else {
          val newCharacter = readResult.toChar
          val newState = previousState.appendNewCharacter(newCharacter)

          newState match {
            case ReadState.DelimiterReached(currentFieldWithoutDelimiter) =>
              if (currentFieldWithoutDelimiter.isFullyQuoted(newState.quotingState))
                newState.updateField(currentFieldWithoutDelimiter.dropQuotation)
              else newState.updateField(currentFieldWithoutDelimiter)
            case ReadState.NewLineReached(currentFieldWithoutNewLine) =>
              if (currentFieldWithoutNewLine.isFullyQuoted(newState.quotingState))
                newState.updateField(currentFieldWithoutNewLine.dropQuotation)
              newState.updateField(newState.currentField.dropNewLine)
            case _ => readField(newState)
          }
        }
      }

      val state = readField(ReadState.Empty)
      val currentField = state.currentField
      val obtainedField =
        if (currentField.isFullyQuoted(state.quotingState)) state.currentField.dropQuotation(parameters)
        else state.currentField
      if (state.isLineTerminated(parameters)) LazyList(obtainedField.value)
      else obtainedField.value #:: readLine
    }

    def readRows: LazyList[LazyList[String]] = {
      if (reader.ready()) readLine #:: readRows else LazyList.empty
    }

    MyCsvContent(if (parameters.containsHeader && reader.ready()) Some(readLine) else None, readRows)
  }
}
