package onedot

import java.io.File
import java.nio.charset.{ Charset, StandardCharsets }

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class MyCsvReaderSpec extends AnyWordSpec with Matchers {
  "My CSV reader" should {
    "read an empty file without errors, when header is expected" in new TestScope {
      override def file: File = readFromResources("/empty_file.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = true)

      csvReadingResult.header shouldBe None
      csvReadingResult.rows.map(_.toList).toList shouldBe List.empty
    }

    "read an empty file without errors, when header is not expected" in new TestScope {
      override def file: File = readFromResources("/empty_file.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      csvReadingResult.header shouldBe None
      csvReadingResult.rows.map(_.toList).toList shouldBe List.empty
    }

    "read a simple file without header" in new TestScope {
      override def file: File = readFromResources("/simple_file_without_header.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      csvReadingResult.header shouldBe None

      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("1", "a1", "b1", "c1"), List("2", "a2", "b2", "c2"))
    }

    "read a simple file with header" in new TestScope {
      override def file: File = readFromResources("/simple_file_with_header.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = true)

      csvReadingResult.header shouldBe Some(List("id", "a", "b", "c"))
      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("1", "a1", "b1", "c1"), List("2", "a2", "b2", "c2"))
    }

    "read a file with custom delimiter" in new TestScope {
      override def file: File = readFromResources("/custom_delimiter.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = true, fieldDelimiter = "fish")

      csvReadingResult.header shouldBe Some(List("id", "a", "b", "c"))
      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("1", "a1", "b1", "c1"), List("2", "a2", "b2", "c2"))
    }

    "read a file with custom new line" in new TestScope {
      override def file: File = readFromResources("/custom_new_line.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = true, lineSeparator = "fish")

      csvReadingResult.header shouldBe Some(List("id", "a", "b", "c"))
      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("1", "a1", "b1", "c1"), List("2", "a2", "b2", "c2"))
    }

    "read a file with custom quotation" in new TestScope {
      override def file: File = readFromResources("/simple_file_with_quotation.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = true, quotingString = "\"")

      csvReadingResult.header shouldBe Some(List("id", "a", "b", "c"))
      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("1", "a1", "b1", "c1"), List("2", "a2", "b2", "c2"))
    }

    "read file with new line inside the quotation" in new TestScope {
      override def file: File = readFromResources("/quotation_with_new_line.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      val expected = List(List("a", "a split\ncell"), List("b", "something else"))

      csvReadingResult.rows.map(_.toList).toList shouldBe expected
    }

    "read file with delimiters inside a quotation" in new TestScope {
      override def file: File = readFromResources("/delimiters_inside_quotation.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("a", "b,c,d", "e"))
    }

    "handle absent values" in new TestScope {
      override def file: File = readFromResources("/absent_values.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("a", "b", ""), List("a", "", "c"))
    }

    "handle a case where a part of field is quoted" in new TestScope {
      override def file: File = readFromResources("/part_of_field_is_quoted.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false)

      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("\"abc,\"onetwo", "three", "doremi"))
    }

    "handle a file with different encoding" in new TestScope {
      override def file: File = readFromResources("/different_encoding.csv")

      override def parameters: CsvParsingParameters = CsvParsingParameters(containsHeader = false, charset = Charset.forName("ISO8859_2"))

      csvReadingResult.rows.map(_.toList).toList shouldBe List(List("żółć", "łódź", "chrząszcz"), List("ręcznik", "ryba", "źli"))
    }
  }

  trait TestScope {
    def file: File
    def parameters: CsvParsingParameters

    val reader = new MyCsvReader()

    lazy val csvReadingResult: CsvContent = reader.read(file, parameters)

    def readFromResources(fileName: String) = new File(getClass.getResource(fileName).getPath)
  }
}
