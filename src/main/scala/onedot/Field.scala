package onedot

case class Field(value: String)

object Field {

  implicit class FieldOps(val field: Field) extends AnyVal {
    def appendCharacter(newCharacter: Char): Field = field.copy(value = field.value.appended(newCharacter))

    def dropNewLine(implicit parameters: CsvParsingParameters): Field =
      field.copy(value = field.value.dropRight(parameters.lineSeparator.length))

    def dropDelimiter(implicit parameters: CsvParsingParameters): Field =
      field.copy(value = field.value.dropRight(parameters.fieldDelimiter.length))

    def dropQuotation(implicit parameters: CsvParsingParameters): Field = {
      val quotingStringLength = parameters.quotingString.length
      field.copy(value = field.value.drop(quotingStringLength).dropRight(quotingStringLength))
    }

    def isFullyQuoted(quotingState: QuotingState)(implicit parameters: CsvParsingParameters): Boolean = {
      val quotedBeginning = quotingState.quoteOpenedAt.contains(parameters.quotingStringLength - 1)
      val quotedEnd = quotingState.quoteClosedAt.contains(field.value.length - parameters.quotingStringLength)

      quotedBeginning && quotedEnd
    }

    def length: Int = field.value.length
  }
}
