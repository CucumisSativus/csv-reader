import java.io.File
import java.nio.charset.{ Charset, StandardCharsets }

package object onedot {

  trait CsvContent { // provide an implementation
    val header: Option[LazyList[String]]
    val rows: LazyList[LazyList[String]]
  }
  abstract class CsvReader {
    def read(path: File, parameters: CsvParsingParameters): CsvContent
    // provide an implementation
  }

  case class CsvParsingParameters(
      containsHeader: Boolean,
      quotingString: String = "\"",
      lineSeparator: String = "\n",
      fieldDelimiter: String = ",",
      charset: Charset = StandardCharsets.UTF_8)

  object CsvParsingParameters {
    implicit class MyCsvParsingParametersOps(val params: CsvParsingParameters) extends AnyVal {
      def quotingStringLength: Int = params.quotingString.length
    }
  }
}
